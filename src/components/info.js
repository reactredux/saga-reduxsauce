import React, { Component } from 'react'
import { connect } from 'react-redux'

// import { loadDataIpRequest, loadDataUaRequest } from '../store/actions'
import ActionCreators from '../store/actionCreators'

export class Info extends Component {

    executarIP = () => {
        this.props.loadDataIP()
    }

    executarUA = () => {
        this.props.loadDataUA()
    }

    render() {

        const loadingIP = this.props.isFetchingIP
        const loadingUA = this.props.isFetchingUA

        return (
            <div>
                <h2>IP</h2>
                <button onClick={() => this.executarIP()}>Obter IP</button>

                {this.props.errorIP &&
                    <div>{this.props.msgErrorIP}</div>
                }

                {!this.props.errorIP &&
                    <div>
                        {loadingIP && <div>Aguarde...</div>}
                        {!loadingIP && <div>{this.props.dataIP}</div>}
                    </div>
                }

                <hr />

                <h2>UA</h2>
                <button onClick={() => this.executarUA()}>Obter User-Agent</button>

                {this.props.errorUA &&
                    <div>{this.props.msgErrorUA}</div>
                }

                {!this.props.errorUA &&
                    <div>
                        {loadingUA && <div>Aguarde...</div>}
                        {!loadingUA && <div>{this.props.dataUA}</div>}
                    </div>
                }
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        isFetchingIP: state.retIpReducer.isFetching,
        dataIP: state.retIpReducer.data,
        errorIP: state.retIpReducer.error,
        msgErrorIP: state.retIpReducer.msgError,

        isFetchingUA: state.retUaReducer.isFetching,
        dataUA: state.retUaReducer.data,
        errorUA: state.retUaReducer.error,
        msgErrorUA: state.retUaReducer.msgError
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        loadDataIP: () => dispatch(ActionCreators.loadDataIpRequest()),
        loadDataUA: () => dispatch(ActionCreators.loadDataUaRequest())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Info)

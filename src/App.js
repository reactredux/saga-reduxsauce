import React, { Component } from 'react';
import './App.css';

import { Provider } from 'react-redux'
import store from './store/index'
import Info from './components/info';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <div className="App">
          <Info />
        </div>
      </Provider>
    );
  }
}

export default App;

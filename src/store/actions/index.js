import { takeLatest, all } from 'redux-saga/effects'
import { Types } from '../actionCreators'
import axios from 'axios'

import { getIP } from '../sagas/ipSagas'
import { getUA } from '../sagas/uaSagas'

export default function* rootSaga() {
    yield all([
        takeLatest(Types.LOAD_DATA_IP_REQUEST, getIP({ axios })),
        takeLatest(Types.LOAD_DATA_UA_REQUEST, getUA({ axios }))
    ])
}
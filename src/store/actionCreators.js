import { createActions } from 'reduxsauce'

export const { Types, Creators } = createActions({
    loadDataIpRequest: [''],
    loadDataIpSuccess: ['data'],
    loadDataIpFailure: ['msg'],

    loadDataUaRequest: [''],
    loadDataUaSuccess: ['data'],
    loadDataUaFailure: ['msg']
})

export default Creators
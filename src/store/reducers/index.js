import { combineReducers } from 'redux'

import ipReducer from './ipReducer'
import uaReducer from './uaReducer'

const rootReducer = combineReducers({
    retIpReducer: ipReducer,
    retUaReducer: uaReducer
})

export default rootReducer
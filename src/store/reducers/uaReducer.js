import { createReducer } from 'reduxsauce'
import { Types } from '../actionCreators'

export const INITIAL_STATE = {
    data: [],
    isFetching: false,
    error: false,
    msgError: ''
}

export const loadDataUaRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isFetching: true,
        error: false,
        msgError: ''
    }
}

export const loadDataUaSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        data: action.data,
        isFetching: false,
        error: false,
        msgError: ''
    }
}

export const loadDataUaFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isFetching: false,
        error: true,
        msgError: action.msg
    }
}

export const HANDLERS = {
    [Types.LOAD_DATA_UA_REQUEST]: loadDataUaRequest,
    [Types.LOAD_DATA_UA_SUCCESS]: loadDataUaSuccess,
    [Types.LOAD_DATA_UA_FAILURE]: loadDataUaFailure
}

export default createReducer(INITIAL_STATE, HANDLERS)
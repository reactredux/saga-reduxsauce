import { createReducer } from 'reduxsauce'
import { Types } from '../actionCreators'

export const INITIAL_STATE = {
    data: [],
    isFetching: false,
    error: false,
    msgError: ''
}

export const loadDataIpRequest = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isFetching: true,
        error: false,
        msgError: ''
    }
}

export const loadDataIpSuccess = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        data: action.data,
        isFetching: false,
        error: false,
        msgError: ''
    }
}

export const loadDataIpFailure = (state = INITIAL_STATE, action) => {
    return {
        ...state,
        isFetching: false,
        error: true,
        msgError: action.msg
    }
}

export const HANDLERS = {
    [Types.LOAD_DATA_IP_REQUEST]: loadDataIpRequest,
    [Types.LOAD_DATA_IP_SUCCESS]: loadDataIpSuccess,
    [Types.LOAD_DATA_IP_FAILURE]: loadDataIpFailure
}

export default createReducer(INITIAL_STATE, HANDLERS)
import { createStore, applyMiddleware } from 'redux'
import logger from 'redux-logger'

import rootReducer from './reducers'

import indexSaga from './actions'

import createSagaMiddleware from 'redux-saga'
const sagaMiddlware = createSagaMiddleware()

const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddlware, logger)
)

store.subscribe(() => {
    console.log('Store update: ', store.getState())
})

sagaMiddlware.run(indexSaga)

export default store
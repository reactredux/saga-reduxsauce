import { put } from 'redux-saga/effects'

import ActionCreators from '../actionCreators'

export const getUA = ({ axios }) => function* () {
    try {
        const dados = yield axios.get('http://httpbin.org/user-agent')
        yield put(ActionCreators.loadDataUaSuccess(dados.data['user-agent']))
    } catch (e) {
        yield put(ActionCreators.loadDataUaFailure(e.message))
    }
}
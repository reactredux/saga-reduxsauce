import { put } from 'redux-saga/effects'

import ActionCreators from '../actionCreators'

export const getIP = ({ axios }) => function* () {
    try {
        const dados = yield axios.get('http://httpbin.org/ip')
        yield put(ActionCreators.loadDataIpSuccess(dados.data.origin))
    } catch (e) {
        yield put(ActionCreators.loadDataIpFailure(e.message))
    }
}